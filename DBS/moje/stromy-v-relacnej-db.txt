nested set model je fajn pre staticky model - je to fajn pre rychle selecty, viem si selectnut cely podstrom ale treba po update precislovat celu tabulku. 

ked si pamatam iba parent id - vyborne sa insertuje a updatuje, ale zvysuje to pocet selectov a nejde selectnut celu hierarchiu

--postgres umoznuje recursive queries
--viem si vytiahnut celu hierarchiu takto
WITH RECURSIVE products_tree AS (
	SELECT id, label, parent_id
	FROM hierarchy.products
	WHERE id = 3
	UNION ALL
	SELECT p.id, p.label, p.parent_id
	FROM hierarchy.products p
	JOIN preducts_tree pt ON p.parent_id = pt.id
)
SELECT * FROM products_tree;