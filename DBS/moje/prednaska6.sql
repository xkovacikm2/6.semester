-- Telo query
-- SELECT a1...an, COUNT, MAX, AVG, SUM
-- FROM t1
-- [JOIN T1, JOIN T2]
-- WHERE cond
-- GROUP BY attrs
-- HAVING cond2

-- pocet ludi, ktori jedli v hornej jedalni
SELECT count(distinct l.student_id)
FROM lunches l
JOIN restaurants r ON r.id = l.restaurant_id
WHERE r.name LIKE 'horna';

-- GROUP BY
SELECT student_id, count(*)
FROM lunches
GROUP BY student_id;

-- celkova kapacita jednotlivych locations
SELECT location, sum(capacity) FROM restaurants GROUP BY location;

-- je dobre groupovat podla Primary Key, inak hrozi nedeterminizmus
SELECT student_id, s.name, count(*)
FROM lunches l 
JOIN students s ON s.id = l.student_id
GROUP BY s.id

--chcem tie location co maju viac ako 2 jedalne
SELECT location, sum(capacity)
FROM restaurants
GROUP BY location
HAVING count(*)>2

-- zmazeme obedy studentom c.6 a c .7
DELETE FROM lunches WHERE student_id = 6 or student_id = 7