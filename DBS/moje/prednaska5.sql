﻿-- Kompletne najoinovane
SELECT s.name, s.vsp, r.name, l.was_tasty
FROM students s
-- JOIN je implicitne INNER JOIN, znamena to ze vynecha
-- riadky, ktore sa nepodari najoinovat
JOIN lunches l ON s.id = l.student_id
JOIN restaurants r ON l.restaurant_id = r.id
-- podmienka filtrovania
WHERE vsp < 3
-- prve zoradovanie je primarne, druhe je sekundarne
ORDER BY s.vsp DESC, r.name ASC;


--chceme dvojice studentov, ktori maju rovnake vsp
SELECT s1.id, s1.name, s1.vsp, s2.id, s2.name, s2.vsp
FROM students s1
JOIN students s2 ON s1.vsp = s2.vsp
--podmienka na odfiltrovanie dvojic sam so sebou a odstranenie duplicit
WHERE s1.id < s2.id;

--ked chcem zjednotit vysledky 2 joinov - UNION, odstranuje duplicity
--UNION ALL nechava duplicity
SELECT name AS label FROM students
UNION
SELECT name FROM restaurants
ORDER BY label;

--v postgrese prienik je INTERSECT, da sa robit cez JOIN, MYSQL ho nema
SELECT student_id FROM lunches WHERE restaurant_id=1
INTERSECT
SELECT student_id FROM lunches WHERE restaurant_id=2;

--iny sposob, cez subselect
SELECT l1.student_id
FROM lunches l1
WHERE l1.restaurant_id = 2 AND student_id IN
(SELECT l2.student_id FROM lunches l2 WHERE l2.restaurant_id = 3);

--postgres ma minus (EXCEPT)
--chceme studentov, ktori obedovali v 1 a nie v 2
SELECT student_id FROM lunches WHERE restaurant_id = 1
EXCEPT
SELECT student_id FROM lunches WHERE restaurant_id = 2;

--MYSQL nema minus, to iste v MYSQL cez subselect
SELECT DISTINCT l1.student_id
FROM lunches l1
WHERE l1.restaurant_id = 1
AND l1.student_id NOT IN
(SELECT l1.student_id
FROM lunches l1
WHERE l1.restaurant_id = 2);

--ziskanie max vsp cez agregacnu funkciu
SELECT max(vsp) FROM students;

--max vsp cez subquery
SELECT s1.id, s1.name, s1.vsp
FROM students s1
WHERE NOT EXISTS (SELECT * FROM students s2 WHERE s1.vsp < s2.vsp);

--vo where condition nejde pouzit custom stplce vyrobene v selecte
--ohackuje double selectom
SELECT *
FROM (SELECT id, name, vsp*2 as double_vsp FROM students)
WHERE double_vsp >4

--GROUP BY sluzi na rozbitie tabulky na viacero tabuliek podla nejako stlpca
--chcem vediet maximalne vsp v jedalnach
SELECT r.name, max(s.vsp)
FROM restaurants r
JOIN lunches l on r.id = l.restaurant_id
JOIN students s on s.id = l.student_id33
GROUP BY r.id
--teraz ked chcem iba jedalne s id <3
HAVING r.id < 3

--vrat mi niekolko prvych -- LIMIT x