﻿CREATE TABLE documents(
  id BIGINT,
  name TEXT,
  type VARCHAR(500),
  created_at DATE,
  department VARCHAR(500),
  customer VARCHAR(500),
  supplier VARCHAR(500),
  supplier_ico VARCHAR(200),
  contracted_amount FLOAT,
  total_amount FLOAT,
  published_on DATE,
  effective_from DATE,
  expires_on DATE,
  note TEXT,
  pages_count SMALLINT,
  source VARCHAR(255)
);

-- v psql
\copy documents(id,name,type,created_at,department,customer,supplier,supplier_ico,contracted_amount,total_amount,published_on,effective_from,expires_on,note,pages_count,source) FROM '/home/barla otvorenezmluvy.csv' WITH CSV HEADER

EXPLAIN SELECT * FROM documents WHERE customer LIKE 'Ministerstvo pôdohospodárstva a rozvoja vidieka SR%'
EXPLAIN SELECT * FROM documents WHERE customer LIKE 'PAPO, s.r.o.'

-- WHY varchar_pattern_ops? http://www.postgresql.org/docs/9.2/static/indexes-opclass.html
CREATE INDEX idx_documents_customer ON documents(customer varchar_pattern_ops);