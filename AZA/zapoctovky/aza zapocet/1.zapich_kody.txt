Ak� je zlo�itos� proced�ry proc(i, sum) ? Nap�te, pre dan� i , �omu sa rovn� sum
na v�stupe, ak na vstupe *sum =0. Svoje tvrdenie dok�te rie�en�m zodpovedaj�cej
rekurentnej rovnice.
void proc(int i, int *sum) {
if (i>1) {
proc(i-1,sum);
proc(i-1,sum);
(*sum)++;
}
else
(*sum)++;
}

Ak� je zlo�itos� proced�ry prog(n) ? Ak� ��slo vyp�e pre dan� ?
void prog(int n) {
int sum=0;
for (int i=0; i<n;i++) {
proc(i,&sum);
sum++;
}
sum++;
printf("%d\n",sum);
}


Vypo��ta� to vlastne ako nehomog�nnu rekurentn� rovnicu a(i) = 2a(i-1) + 1, kde a(i) je velkos� sum 
pre ur�it� i, 2 kr�t vol� rekurz�vne a(i-1) a +1 je inkrement�cia sum. Vyjs� by to malo a(i) = (2^i) - 1....
v druhej �asti vol� t� proced�ru proc nkr�t...tak�e jednoducho vypo��ta� rad od 0 po n-1 z 2^i....to vyjde (2^n)-1...
tam e�te pripo��ta� t� jedn� inkrement�ciu na konci tak�e vysledok je 2^n