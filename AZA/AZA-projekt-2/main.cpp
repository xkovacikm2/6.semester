/* 
 * File:   main.cpp
 * Author: Michal Kováčik
 *
 * Created on May 2, 2016, 11:15 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <queue>
#define OPTIMIZE __attribute__((optimize("O2")))

using namespace std;

enum Direction {
    NORTH, EAST, SOUTH, WEST
};

class Graph {
private:
    int **paths;
public:
    int size;

    Graph(int _size) : size(_size) {
        this->paths = (int**) malloc(_size * 2 * sizeof (int*));
        for (int i = 0, lim = 2 * _size; i < lim; i++) {
            this->paths[i] = (int*) malloc(_size * sizeof (int));
        }
    }

    OPTIMIZE void loadPaths() {
        int temp;
        for (int y = 0; y<this->size; y++) {
            for (int i = 1; i <= 2; i++) {
                if ((y == this->size - 1) && i == 2) break;
                for (int x = 0; x<this->size - (i % 2); x++) {
                    scanf("%d", &temp);
                    this->paths[2 * y + (i - 1)][x] = temp;
                }
            }
        }
    }

    OPTIMIZE int getPathSize(int y, int x, Direction d) {
        int i, j;
        switch (d) {
            case WEST:
                if (x <= 0) return -1;
                i = 2 * y;
                j = x - 1;
                break;
            case EAST:
                if (x >= this->size - 1) return -1;
                i = 2 * y;
                j = x;
                break;
            case NORTH:
                if (y <= 0) return -1;
                i = 2 * (y - 1) + 1;
                j = x;
                break;
            case SOUTH:
                if (y >= this->size - 1) return -1;
                i = 2 * y + 1;
                j = x;
                break;
        }
        return this->paths[i][j];
    }

    OPTIMIZE bool isPeriphery(int y, int x) {
        return (y == 0 || x == 0 || y == this->size - 1 || x == this->size - 1);
    }

    OPTIMIZE bool isTarget(int y, int x) {
        return (y == this->size - 1 && x == this->size - 1);
    }
};

class State {
public:
    int x;
    int y;
    Direction from;
    int path;
    int value;
    Graph *g;
    bool from_start;

    State(int _x, int _y, Direction _from, int _path, Graph *_g, bool _from_start) : x(_x), y(_y), from(_from), path(_path), g(_g), from_start(_from_start) {
        this->value = ((2 * (g->size - 1) - this->x - this->y)*3 * (int) _from_start) + this->path;
    }

    OPTIMIZE vector<State*> *generateFollowers() {
        vector<State*> *vec = new vector<State*>();
        vec->reserve(4);
        Direction forbidden;
        if (this->from_start)
            forbidden = this->leftTurn();
        else
            forbidden = this->rightTurn();
        for (int i = 0; i < 4; i++) {
            if (i == this->from || (!this->g->isPeriphery(this->y, this->x) && i == forbidden)) continue;
            int x, y, path;
            switch ((Direction) i) {
                case NORTH:
                    x = this->x;
                    y = this->y - 1;
                    path = this->g->getPathSize(this->y, this->x, NORTH);
                    break;
                case EAST:
                    x = this->x + 1;
                    y = this->y;
                    path = this->g->getPathSize(this->y, this->x, EAST);
                    break;
                case SOUTH:
                    x = this->x;
                    y = this->y + 1;
                    path = this->g->getPathSize(this->y, this->x, SOUTH);
                    break;
                case WEST:
                    x = this->x - 1;
                    y = this->y;
                    path = this->g->getPathSize(this->y, this->x, WEST);
            }

            if (path == -1) continue;
            vec->push_back(new State(x, y, (Direction) ((i + 2) % 4), this->path + path, this->g, this->from_start));
        }
        return vec;
    }

    OPTIMIZE bool isFinal() {
        return g->isTarget(this->y, this->x);
    }

    OPTIMIZE bool isPeriphery() {
        return g->isPeriphery(this->y, this->x);
    }

    OPTIMIZE int toHash() {
        return 4 * (x * g->size + y)+(int) this->from;
    }

    OPTIMIZE int toOp1Hash() {
        return 4 * (x * g->size + y)+(((int) this->from + 2) % 4);
    }

    OPTIMIZE int toOp2Hash() {
        int rv = 4 * (x * g->size + y);
        if (this->from_start)
            rv = rv + (int) (this->rightTurn());
        else
            rv = rv + (int) (this->leftTurn());
        return rv;
    }

    OPTIMIZE int toOp3Hash() {
        int rv = 4 * (x * g->size + y);
        if (!this->from_start)
            rv = rv + (int) (this->rightTurn());
        else
            rv = rv + (int) (this->leftTurn());
        return rv;
    }
private:

    Direction leftTurn() {
        return (Direction) (((int) this->from + 1) % 4);
    }

    Direction rightTurn() {
        return (Direction) (((int) this->from + 3) % 4);
    }
};

class CompareState {
public:

    bool operator()(State*& s1, State*& s2) {
        return s2->value < s1->value;
    }
};

OPTIMIZE int AStarStep(State **hmap, priority_queue<State*, vector<State*>, CompareState> *pq1, bool *visited1) {
    State *s1 = pq1->top();
    pq1->pop();
    if (hmap[s1->toOp1Hash()] != NULL && s1->from_start != hmap[s1->toOp1Hash()]->from_start)
        return hmap[s1->toOp1Hash()]->path + s1->path;
    if (hmap[s1->toOp2Hash()] != NULL && s1->from_start != hmap[s1->toOp2Hash()]->from_start)
        return hmap[s1->toOp2Hash()]->path + s1->path;
    if (s1->isPeriphery() && hmap[s1->toOp3Hash()] != NULL && s1->from_start != hmap[s1->toOp3Hash()]->from_start)
        return hmap[s1->toOp3Hash()]->path + s1->path;
    hmap[s1->toHash()] = s1;
    vector<State*> *vec = s1->generateFollowers();
    for (vector<State*>::iterator itn = vec->begin(); itn != vec->end(); itn++) {
        if (visited1[(*itn)->toHash()] == false) {
            pq1->push(*itn);
            visited1[(*itn)->toHash()] = true;
        }
    }
    return -1;
}

OPTIMIZE int AStar(Graph *g) {
    State *s1 = new State(0, 0, NORTH, 0, g, true);
    State *s2 = new State(g->size - 1, g->size - 1, SOUTH, 0, g, false);

    int bsize = 4 * g->size * g->size;
    bool visited1[bsize];
    bool visited2[bsize];
    State * hmap[bsize];
    for (int i = 0; i < bsize; i++) {
        visited1[i] = false;
        visited2[i] = false;
        hmap[i] = NULL;
    }

    vector < State *> vec1;
    vec1.reserve(bsize);
    vec1.push_back(s1);
    priority_queue<State*, vector<State*>, CompareState> pq1(vec1.begin(), vec1.end());
    vector < State *> vec2;
    vec2.reserve(bsize);
    vec2.push_back(s2);
    priority_queue<State*, vector<State*>, CompareState> pq2(vec2.begin(), vec2.end());

    while (!pq1.empty() && !pq2.empty()) {
        int rv = AStarStep(hmap, &pq1, visited1);
        if (rv != -1) return rv;
        rv = AStarStep(hmap, &pq2, visited2);
        if (rv != -1) return rv;
    }
    return -1;
}

OPTIMIZE int main() {
    int n;
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        int k;
        scanf("%d", &k);
        Graph *g = new Graph(k);
        g->loadPaths();
        printf("%d\n", AStar(g));
    }
    return 0;
}