/* 
 * File:   main.cpp
 * Author: Michal Kováčik
 *
 * Created on April 12, 2016, 7:45 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <queue>
#include <iostream>

using namespace std;

class vertex {
public:
    int from;
    int to;

    vertex(int _from, int _to) : from(_from), to(_to) {
    }
};

class node {
public:
    queue<vertex> v;
    int p;
    int index;
    bool queued;
    int inCycle;
};

queue<int>* q = NULL;
vector<node*>* g = NULL;
vector<int> *inCycleVec = NULL;
int smallest = -1;

void loadGraph();
void printGraph();
void findSmallest();
int findLoop(node *n, node *parent);
bool cmpLoop(int newLoop, int oldLoop);
int findCommonParent(int node);
int pathToCommonParent(int node, int parent);
void cleanInCycleFlag();

int main() {
    int graphs;
    scanf("%d", &graphs);
    for (int i = 0; i < graphs; i++) {
        loadGraph();
        findSmallest();
        printf("%d\n", smallest);
        smallest = -1;
    }
    return 0;
}

void loadGraph() {

    int n_nodes;
    scanf("%d", &n_nodes);

    g = new vector<node*>();
    q = new queue<int>();
    g->reserve(n_nodes);

    for (int i = 0; i < n_nodes; i++) {
        int n_vertices;
        scanf("%d", &n_vertices);
        node *n = new node();
        n->index = i;
        n->p = -1;
        for (int j = 0; j < n_vertices; j++) {
            int to;
            scanf("%d", &to);
            vertex v(j, to - 1);
            n->v.push(v);
        }
        g->push_back(n);
    }

    g->front()->queued = true;
    q->push(g->front()->index);
}

void findSmallest() {
    int depth = 0;
    while (!q->empty()) {
        node *n = g->at(q->front());
        q->pop();
        while (!n->v.empty()) {
            vertex v = n->v.front();
            n->v.pop();
            if (v.to == n->p) continue;
            int loopSize = findLoop(g->at(v.to), n);

            if (!g->at(v.to)->queued) {
                g->at(v.to)->queued = true;
                q->push(v.to);
            }

            if (loopSize != -1 && cmpLoop(loopSize, smallest)) {
                smallest = loopSize;
            }

            if (smallest == 3) return;
        }
        depth++;
    }
}

int findLoop(node *n, node *parent) {
    if (n->p == -1) {
        n->p = parent->index;
        return -1;
    }
    inCycleVec = new vector<int>();
    if (smallest != -1) {
        inCycleVec->reserve(2 * smallest);
    }
    findCommonParent(n->index);
    int size = findCommonParent(parent->index);
    cleanInCycleFlag();
    n->p = parent->index;
    return size;
}

int findCommonParent(int node) {
    int size = 0;
    while (g->at(node)->inCycle==0) {
        size++;
        g->at(node)->inCycle = size;
        inCycleVec->push_back(node);
        if (!cmpLoop(size, smallest)) return -1;
        if (node == 0) break;
        node = g->at(node)->p;
    }
    return size+g->at(node)->inCycle;
}

void cleanInCycleFlag() {
    for (vector<int>::iterator itn = inCycleVec->begin(); itn != inCycleVec->end(); itn++) {
        g->at(*itn)->inCycle = 0;
    }
}

bool cmpLoop(int newLoop, int oldLoop) {
    if (oldLoop == -1) return true;
    return newLoop<oldLoop;
}