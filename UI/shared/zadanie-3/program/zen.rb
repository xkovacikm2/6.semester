require File.dirname(__FILE__) + '/monk'
require File.dirname(__FILE__) + '/garden'
require File.dirname(__FILE__) + '/gene'

population_size = 15
generations = 15
annealing_probability = 0.3
mutex_threads = Mutex.new
mutex_counter = Mutex.new
cv_count = 1
cv = ConditionVariable.new

population = []
statistics = []

selection = (ARGV[0].nil?) ? 'crop' : ARGV[0]

population_size.times do
  population.push Monk.new(Garden.new)
end

case selection

  when 'crop'
    generations.times do |i|
      cv.wait if cv_count < 1
      cv_count = 0
      population = population.sort_by { |monk| monk.value }.reverse.slice!(0..(population_size/10))

      puts "generation #{i} best score #{population[0].value}"
      statistics.push population[0].value

      break if population[0].value == 500

      if rand < annealing_probability
        100.times do
          population.push Monk.new(Garden.new)
        end
      end

      orig_size = population_size-population.size
      tcount=0;
      (1..4).each do
        Thread.start do
          (orig_size/4).times do
            monk=Monk.new(Garden.new, [population[rand orig_size], population[rand orig_size]])
            mutex_threads.synchronize do
              population.push monk
            end
          end
          mutex_counter.synchronize do
            tcount=tcount+1
            cv.signal if tcount == 4
          end
        end
      end
    end

  when 'roulette'
    generations.times do |i|
      val_sum=population.inject(0) { |sum, monk| sum + monk.value }
      new_population=[]
      (population_size/10).times do
        roulette = rand val_sum
        ind = population.each_with_index.inject(0) do |sum, (monk, index)|
          break index-1 if sum > roulette
          sum + monk.value
        end
        new_population.push population[ind] unless population[ind].nil?
      end

      puts "generation #{i} average #{val_sum/population_size}"
      statistics.push val_sum/population_size

      population = new_population
      orig_size = population.size
      (population_size-population.size).times do
        r1=rand orig_size-1
        r2=rand orig_size-1
        population.push Monk.new(Garden.new, [population[r1], population[r2]])
      end
    end

  else
    abort 'wrong argument'
end

population = population.sort_by { |monk| monk.value }.reverse.slice!(0..3)

puts population.size

population.each do |monk|
  puts "evaluation #{monk.value}"
  monk.map.visualise
  puts " "
end

if !ARGV[1].nil? and ARGV[1] == 'generate-diagram'
  require 'gchart'
  Gchart.line(
      :title => "narast fitness",
      :bg => 'efefef',
      :data => statistics,
      axis_with_labels: 'x,y',
      axis_labels: ['generation', 'fitness'],
      axis_range: [[0, statistics.size, 10], [statistics[0], statistics[-1], 20]],
      format: 'file',
      filename: 'chart.png')
end
