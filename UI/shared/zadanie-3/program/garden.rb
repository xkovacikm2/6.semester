require File.dirname(__FILE__) + '/monk'
require File.dirname(__FILE__) + '/gene'

class Garden

  def initialize
    @map=[
        %w(0 0 0 0 0 0 0 0 0 0 0 0),
        %w(0 0 0 0 0 K 0 0 0 0 0 0),
        %w(0 K 0 0 0 0 0 0 0 0 0 0),
        %w(0 0 0 0 K 0 0 0 0 0 0 0),
        %w(0 0 0 0 0 0 0 0 0 0 0 0),
        %w(0 0 0 0 0 0 0 0 0 0 0 0),
        %w(0 0 0 0 0 0 0 0 K K 0 0),
        %w(0 0 0 0 0 0 0 0 0 0 0 0),
        %w(0 0 0 0 0 0 0 0 0 0 0 0),
        %w(0 0 0 0 0 0 0 0 0 0 0 0)
    ]
    @zeros = count_all '0'
  end

  def entrances
    @map.size*2+@map[0].size*2
  end

  def stones
    return @n_stones unless @n_stones.nil?
    @n_stones=count_all 'K'
  end

  def evaluate(monk)
    return @value unless @value.nil?
    walk_map monk
    zeros_a_w = count_all '0'
    value = (1-(zeros_a_w.to_f/@zeros))*300
    @value=(value == 300)? 500:value
  end

  def visualise
    @map.each do |row|
      puts row.to_s
    end
  end

  private
  def coordinates_from_entrance_id(entrance)
    rows = @map.size
    row_size= @map[0].size
    if (entrance-row_size)<0
      return {x: entrance, y:0, direction: :down}
    end
    entrance-=row_size
    if (entrance-rows)<0
      return {x: row_size-1, y: entrance, direction: :left}
    end
    entrance-=rows
    if (entrance-row_size)<0
      return {x: entrance, y:rows-1, direction: :up}
    end
    entrance-=row_size
    {x: 0, y: entrance-1, direction: :right}
  end

  def count_all(sym)
    count=0
    @map.each do |line|
      count+=line.count(sym)
    end
    count
  end

  def walk_map(monk)
    monk.genes.each_with_index do |gene, i|
      coords = coordinates_from_entrance_id gene.entry
      break unless move coords, gene.decisions, i+1
    end
  end

  def move(coords, decisions, i)
    decision=0
    while in_bounds coords
      return false unless @map[coords[:y]][coords[:x]] == '0' or @map[coords[:y]][coords[:x]] == i.to_s
      @map[coords[:y]][coords[:x]] = i.to_s
      case coords[:direction]
        when :down
          modifier = {x: 0, y: +1}
        when :left
          modifier = {x: -1, y: 0}
        when :right
          modifier = {x: +1, y: 0}
        when :up
          modifier = {x: 0, y: -1}
      end
      new_coords={x: coords[:x]+modifier[:x], y: coords[:y]+modifier[:y], direction: coords[:direction]}
      break unless in_bounds new_coords
      unless coords_valid new_coords
        new_coords=change_direction coords, decisions[decision]
        decision+=1
        return false if decision>decisions.size
      end
      coords=new_coords
    end
    true
  end

  def in_bounds(coords)
    coords[:x]>=0 && coords[:y]>=0 && coords[:x]<@map[0].size && coords[:y]<@map.size
  end

  def coords_valid(coords)
    @map[coords[:y]][coords[:x]]=='0'
  end

  def change_direction(coords, decision)
    if (coords[:direction]==:up || coords[:direction]==:down)
      coords[:direction]=(decision==0)? :left : :right
    else
      coords[:direction]=(decision==0)? :up : :down
    end
    coords
  end
end