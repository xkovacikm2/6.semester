require File.dirname(__FILE__) + '/garden'
require File.dirname(__FILE__) + '/gene'

class Monk
  attr_accessor :map
  attr_accessor :genes

  def initialize(map, parents=nil)
    @genes = []
    @map = map
    if parents.nil?
      (map.entrances+map.stones).times do
        @genes.push Gene.new map.entrances
      end
    else
      n_genes=(parents[0].genes.count+parents[1].genes.count)/2
      n_genes.times do |i|
        @genes.push Gene.new map.entrances, parent1: parents[0].genes[i], parent2: parents[1].genes[i]
      end
    end
  end

  def value
    self.map.evaluate self
  end
end