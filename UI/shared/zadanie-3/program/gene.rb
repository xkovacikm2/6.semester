require File.dirname(__FILE__) + '/monk'
require File.dirname(__FILE__) + '/garden'

class Gene

  attr_accessor :entry
  attr_accessor :decisions

  @@mutation_probability = 0.03
  @@recombination_probability = 0.5

  def initialize(entry, parents=nil)
    if parents.nil?
      @entry = rand entry
      @decisions = []
      20.times do
        @decisions.push rand 2
      end
    else
        @entry = mutate_entry entry, parents
        @decisions = mutate_decisions parents
    end
  end

  private
	def mutate_entry(entry, parents)
	return rand entry if rand < @@mutation_probability
	parents[:parent1].entry
	end

	def mutate_decisions(parents)
	  decisions = parents[:parent1].decisions.clone
	  decisions.each_index do |i|
	    if rand < @@recombination_probability
	      decisions[i]=parents[:parent2].decisions[i]
	    end
	  end
	end
end