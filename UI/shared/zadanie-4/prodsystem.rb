require File.dirname(__FILE__) + '/helper'

facts = load_facts 'facts.txt'
rules = load_rules 'rules.txt'

puts facts

until gets.nil? do
  applicable_actions = purify facts, get_applicable_actions(facts, rules)
  puts applicable_actions
  facts = apply_actions applicable_actions, facts
  puts facts
end
