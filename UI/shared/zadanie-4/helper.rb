def load_facts(file='facts.txt')
  facts=[]
  File.open(file).each_line do |line|
    facts.push line.tr("()\n", '')
  end
  facts
end

def load_rules(file='rules.txt')
  rules=[]
  fr = File.open file
  begin
    rule={}
    rule[:name] = fr.gets.tr(':', '')
    rule[:conditions] = parse_rule(fr.gets).join ' '
    rule[:actions] = parse_rule fr.gets
    rules.push rule
  end until fr.gets.nil?
  fr.close
  rules
end

def parse_rule(rule='')
  rule.split(' ')[1..-1].join(' ').tr('(', '').split(')')
end

def get_applicable_actions(facts, rules)
  mixed_facts= combine_facts facts
  applicable_actions = []
  rules.each do |rule|
    mixed_facts.each do |mixed_fact|
      rsplit = rule[:conditions].split(' ')
      next unless matches mixed_fact, rule[:conditions] or rsplit.include? '<>'
      vars = {}
      flag = true

      mixed_fact.split(' ').each_with_index do |word, idx|
        if rsplit[idx] =~ /\?.*/
          if vars[rsplit[idx]].nil?
            vars[rsplit[idx]] = word
          elsif vars[rsplit[idx]] != word
            flag = false
            break
          end
        elsif word!=rsplit[idx]
          flag=false
          break
        end
      end

      flag = false if rsplit.include? '<>' and rsplit[-1] == rsplit[-2]
      next unless flag

      rule[:actions].clone.each do |action|
        vars.each do |key, val|
          action = action.gsub key, val
        end
        applicable_actions.push action
      end
    end
  end
  applicable_actions
end

def combine_facts(facts)
  mixed_facts = []
  facts.each do |part1|
    facts.each do |part2|
      mixed_facts.push part1 + ' ' + part2 if part1 != part2
    end
  end
  mixed_facts
end

def matches(fact, rule)
  fsplit = fact.split(' ')
  rsplit = rule.split(' ')
  return false unless fsplit.size == rsplit.size
  rsplit.each_with_index do |word, idx|
    next if word =~ /\?.*/
    return false if word != fsplit[idx]
  end
  true
end

def purify(facts, actions)
  pure=[]
  actions.each do |action|
    pure.push action if action.split(' ')[0]=='pridaj' and !facts.include? action.split(' ')[1..-1].join(' ')
    pure.push action if action.split(' ')[0]=='vymaz' and facts.include? action.split(' ')[1..-1].join(' ')
    pure.push action if action.split(' ')[0]=='sprava' and pure.any?
  end
  pure
end

def apply_actions(actions, facts)
  actions.each do |action|
    asplit = action.split(' ')
    rest = asplit[1..-1].join(' ')
    if asplit[0]=='pridaj'
      facts.push rest
    elsif asplit[0] == 'vymaz'
      facts.delete rest
    elsif asplit[0] == 'sprava'
      puts "sprava: #{rest}"
    end
  end
  facts
end
